package com.xnx3.wangmarket.admin.vo.bean;

/**
 * 通用模版相关的Bean
 * @author max
 */
public class TemplateCommon {
	private String topHtml;
	private String footHtml;
	public String getTopHtml() {
		return topHtml;
	}
	public void setTopHtml(String topHtml) {
		this.topHtml = topHtml;
	}
	public String getFootHtml() {
		return footHtml;
	}
	public void setFootHtml(String footHtml) {
		this.footHtml = footHtml;
	}
	
	
}
